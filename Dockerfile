FROM adoptopenjdk/openjdk15:alpine-jre
MAINTAINER Miguel Hernández

ARG JAR_FILE
COPY target/${JAR_FILE} /usr/share/azeti/app.jar

ENTRYPOINT ["java","-jar","/usr/share/azeti/app.jar"]