### Overview

The application solves the problem of the greatest-app ever!

In order to run the app you have th run the following commands:
```shell
mvn clean package
docker-compose up
```

That's it! Now you'll be able to access the greatest app ever at http://localhost:80 :)

### Architectural considerations

The project is built using Spring WebFlux. It is also using a single-module implementation of the Clean Architecture
explained by Robert C. Martin.

I also designed the API in the following manner:
```shell
/api/v1/title?pageAddress={pageAddress}
```

I opted out to make pageAddress a query param because it would comply better with the REST contract

### Things to improve

- The application has tests for the business logic, which is stored in the `usecase` package. However, I would have liked
to add some integration tests, while mocking the remote address using something like `WireMock`
- The frontend is not tested at all. I would have liked to use `@testing-library` to write component tests and `Cypress` to write e2e tests
- Some kind of caching. The naive implementation would have been to use a `ConcurrentHashMap` which maybe would have need
to be cleaned every day; this is naive because the page might change title in the meanwhile, and the information in the cache
  would stop being correct
- It also would have been good to have it deployed, and make use of GitLab CI  