package net.azeti.greatestappever.domain;

import java.util.Objects;
import java.util.StringJoiner;

public class PageTitle {

    private final String title;

    public PageTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PageTitle pageTitle = (PageTitle) o;
        return Objects.equals(title, pageTitle.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PageTitle.class.getSimpleName() + "[", "]")
                .add("title='" + title + "'")
                .toString();
    }
}
