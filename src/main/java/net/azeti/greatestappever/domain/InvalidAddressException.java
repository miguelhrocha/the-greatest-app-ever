package net.azeti.greatestappever.domain;

public class InvalidAddressException extends IllegalArgumentException {

    public InvalidAddressException(final String address) {
        super(String.format("The address with URL %s is invalid", address));
    }

}
