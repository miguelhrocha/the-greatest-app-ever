package net.azeti.greatestappever.domain;

public class MissingProtocolException extends IllegalArgumentException {

    public MissingProtocolException() {
        super("Missing protocol, please prepend either HTTP or HTTPS according to your needs");
    }
}
