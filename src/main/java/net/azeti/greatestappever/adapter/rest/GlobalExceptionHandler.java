package net.azeti.greatestappever.adapter.rest;

import net.azeti.greatestappever.adapter.rest.dto.ErrorDto;
import net.azeti.greatestappever.domain.InvalidAddressException;
import net.azeti.greatestappever.domain.MissingProtocolException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Optional;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(InvalidAddressException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorDto> invalidAddressExceptionHandler(final InvalidAddressException exception) {
        return ResponseEntity.of(Optional.of(new ErrorDto(exception.getMessage())));
    }

    @ExceptionHandler(MissingProtocolException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorDto> missingProtocolException(final MissingProtocolException exception) {
        return ResponseEntity.of(Optional.of(new ErrorDto(exception.getMessage())));
    }

}
