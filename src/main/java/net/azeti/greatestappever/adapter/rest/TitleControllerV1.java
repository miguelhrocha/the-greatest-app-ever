package net.azeti.greatestappever.adapter.rest;

import net.azeti.greatestappever.domain.PageTitle;
import net.azeti.greatestappever.usecase.FindPageTitleUseCase;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/title")
public class TitleControllerV1 {

    private final FindPageTitleUseCase findPageTitleUseCase;

    public TitleControllerV1(final FindPageTitleUseCase findPageTitleUseCase) {
        this.findPageTitleUseCase = findPageTitleUseCase;
    }

    @GetMapping
    public Mono<PageTitle> getTitleOfPage(@RequestParam("pageAddress") final String pageAddress) {
        final var sanitizedInput = Jsoup.clean(pageAddress, Whitelist.basic());
        return Mono.fromSupplier(() -> findPageTitleUseCase.execute(sanitizedInput));
    }

}
