package net.azeti.greatestappever.usecase;

import net.azeti.greatestappever.domain.InvalidAddressException;
import net.azeti.greatestappever.domain.MissingProtocolException;
import net.azeti.greatestappever.domain.PageTitle;
import net.azeti.greatestappever.infrastructure.dataprovider.PageTitleSupplier;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class FindPageTitleUseCase implements UseCase<String, PageTitle> {

    private final PageTitleSupplier jsoupPageTitleSupplier;

    public FindPageTitleUseCase(final PageTitleSupplier jsoupPageTitleSupplier) {
        this.jsoupPageTitleSupplier = jsoupPageTitleSupplier;
    }

    @Override
    public PageTitle execute(String request) {
        if (request.startsWith("http://") || request.startsWith("https://")) {
            try {
                final var title = jsoupPageTitleSupplier.supplyPageTitle(request);
                return new PageTitle(title);
            } catch (IOException e) {
                throw new InvalidAddressException(request);
            }
        }
        throw new MissingProtocolException();
    }

}
