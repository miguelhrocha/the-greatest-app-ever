package net.azeti.greatestappever.usecase;

@FunctionalInterface
public interface UseCase<In, Out> {

    Out execute(In request);

}
