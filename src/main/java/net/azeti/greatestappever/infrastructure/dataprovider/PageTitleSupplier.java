package net.azeti.greatestappever.infrastructure.dataprovider;

import java.io.IOException;

public interface PageTitleSupplier {

    String supplyPageTitle(final String pageAddress) throws IOException;

}
