package net.azeti.greatestappever.infrastructure.dataprovider.jsoup;

import net.azeti.greatestappever.infrastructure.dataprovider.PageTitleSupplier;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
class JsoupPageTitleSupplier implements PageTitleSupplier {

    @Override
    public String supplyPageTitle(String pageAddress) throws IOException {
        return Jsoup.connect(pageAddress).get().title();
    }

}
