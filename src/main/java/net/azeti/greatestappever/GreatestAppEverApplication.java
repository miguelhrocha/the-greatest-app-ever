package net.azeti.greatestappever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreatestAppEverApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreatestAppEverApplication.class, args);
	}

}
