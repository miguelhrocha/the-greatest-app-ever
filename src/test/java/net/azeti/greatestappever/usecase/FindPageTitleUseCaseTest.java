package net.azeti.greatestappever.usecase;

import net.azeti.greatestappever.domain.InvalidAddressException;
import net.azeti.greatestappever.domain.MissingProtocolException;
import net.azeti.greatestappever.domain.PageTitle;
import net.azeti.greatestappever.infrastructure.dataprovider.PageTitleSupplier;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FindPageTitleUseCaseTest {

    @Mock
    private PageTitleSupplier pageTitleSupplier;

    @InjectMocks
    private FindPageTitleUseCase systemUnderTest;

    @Test
    void shouldReturnFetchedPageTitle() throws IOException {
        final var address = "http://azeti.net";
        final var expectedTitle = "IoT Application Enablement Platform - UNLOCK THE VALUE OF YOUR DATA";
        when(pageTitleSupplier.supplyPageTitle(address)).thenReturn(expectedTitle);
        final var expectedResult = new PageTitle(expectedTitle);

        final var actualResult = systemUnderTest.execute(address);

        assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Test
    void shouldThrowMissingProtocolExceptionWhenAddressIsMissingHTTPorHTTPS() {
        final var address = "azeti.net";

        assertThrows(MissingProtocolException.class, () -> {
            systemUnderTest.execute(address);
        });
    }

    @Test
    void shouldThrowInvalidAddressExceptionWhenSupplierCanNotProvideTitle() throws IOException {
        final var address = "https://w.not-azeti.net";
        when(pageTitleSupplier.supplyPageTitle(address)).thenThrow(IOException.class);

        assertThrows(InvalidAddressException.class, () -> {
            systemUnderTest.execute(address);
        });
    }

}