import axios from 'axios'

const fetchPageAddress = async (pageAddress: string): Promise<string> => {
  const response = await axios.get<{ title: string }>('/api/v1/title', {
    params: {
      pageAddress: pageAddress,
    },
    baseURL: process.env.REACT_APP_BASE_URL,
  })

  return response.data.title
}

export default fetchPageAddress
