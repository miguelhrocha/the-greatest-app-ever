import { FC } from 'react';
import { Card, CardContent, Typography } from '@material-ui/core'

const FoundTitle: FC<{ title: string }> = ({ title }) => (
    <Card>
        <CardContent>
            <Typography color='textSecondary' gutterBottom>
                Title found
            </Typography>
            <Typography variant='h4' component='h2'>
                { title }
            </Typography>
        </CardContent>
    </Card>
)

export default FoundTitle;
