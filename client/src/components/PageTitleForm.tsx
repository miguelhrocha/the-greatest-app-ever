import { FC } from 'react'
import { useFormik } from 'formik'
import { Button, TextField, makeStyles } from '@material-ui/core'
import * as yup from 'yup'

export type PageTitleFormProps = {
  handleSubmit: (address: string) => Promise<void>
}

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      marginTop: theme.spacing(3),
      marginBottom: theme.spacing(2),
    },
  },
}))

const SearchAddressSchema = yup.object().shape({
  address: yup.string().url('not a valid URL!'),
})

const PageTitleForm: FC<PageTitleFormProps> = ({ handleSubmit }) => {
  const classes = useStyles()
  const formik = useFormik<{ address: string }>({
    initialValues: {
      address: '',
    },
    validationSchema: SearchAddressSchema,
    onSubmit: async ({ address }) => {
      await handleSubmit(address)
    },
  })

  return (
    <form onSubmit={formik.handleSubmit} className={classes.root}>
      <TextField
        fullWidth
        variant="outlined"
        name="address"
        label="Page Address"
        value={formik.values.address}
        onChange={formik.handleChange}
        error={formik.touched.address && Boolean(formik.errors.address)}
        helperText={formik.touched.address && formik.errors.address}
      />
      <Button color="primary" variant="contained" fullWidth type="submit">
        Search address
      </Button>
    </form>
  )
}

export default PageTitleForm
