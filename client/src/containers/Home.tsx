import { FC, useState } from 'react'
import { Container } from '@material-ui/core'
import { CircularProgress, makeStyles } from '@material-ui/core';

import PageTitleForm from 'components/PageTitleForm'
import FoundTitle from 'components/FoundTitle'
import { fetchPageAddress } from '../util'

const useStyles = makeStyles((theme) => ({
  root: {
    '.MuiCard-root': {
      marginTop: theme.spacing(2)
    },
  }
}))

const Home: FC = () => {
  const classes = useStyles();
  const [title, setTitle] = useState<string | null>(null)
  const [loading, setLoading] = useState(false)

  const handleFormSubmit = async (address: string) => {
    setLoading(true);
    const pageTitle = await fetchPageAddress(address)
    setTitle(pageTitle)
    setLoading(false)
  }

  return (
    <div className={classes.root}>
      <Container maxWidth="md">
        <PageTitleForm handleSubmit={handleFormSubmit}></PageTitleForm>
        {title && <FoundTitle title={title} />}
        {loading && <CircularProgress />}
      </Container>
    </div>
  )
}

export default Home
